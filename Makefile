PROJECT_LIST_SERVICES += mysql
PROJECT_NAME ?= mysql

SHOW_CMD_HELP ?= @echo male $(1):
SHOW_TITLE_HELP ?= @echo "\t****** [ $(1) ] ******":
SHOW_TITLE_TARGET ?= @echo "\t****** $(1) ******":

TARGETS_HELP 		+=  mysql-help
PRE_TARGETS_ENV 	+=  mysql-env
PRE_TARGETS_UP		+= mysql-up
PRE_TARGETS_START	+= mysql-start
PRE_TARGETS_STOP	+= mysql-stop
PRE_TARGETS_DOWN	+= mysql-down
PRE_TARGETS_DUMP	+= mysql-dump
PRE_TARGETS_RESTORE	+= mysql-restore

ENV ?= dev
ENV_PATH ?= .
SERVICE_PATH ?= ..
SERVICE_MYSQL_PATH ?= $(SERVICE_PATH)/mysql

MYSQL_COMPOSE_FILE_NAME ?= $(SERVICE_MYSQL_PATH)/docker-compose
MYSQL_COMPOSE_FILE ?= -f $(MYSQL_COMPOSE_FILE_NAME).yml -f $(MYSQL_COMPOSE_FILE_NAME)-$(MODE).$(ENV).yml
PROJECT_COMPOSE_FILE += $(MYSQL_COMPOSE_FILE)

PROJECT_DUMP_PATH ?= $(SERVICE_MYSQL_PATH)


SERVICE_MYSQL_ROOT_PASSWORD ?= SuperSecret
SERVICE_MYSQL_DATABASE ?= example
SERVICE_MYSQL_USER ?= example
SERVICE_MYSQL_PASSWORD ?= example
MYSQL_DUMP_FILE ?= $(PROJECT_DUMP_PATH)/mysql-dumpbase.sql

mysql-help:
	$(call SHOW_TITLE_HELP, MYSQL)
	$(call SHOW_CMD_HELP, mysql-up) Запускаем окружение UP \(MySQL\)
	$(call SHOW_CMD_HELP, mysql-start) Запускаем/стартуем остановленное приложение \(MySQL\)
	$(call SHOW_CMD_HELP, mysql-restore) востановление кластера MySQL\(залить дамп базы в кластер\)
	$(call SHOW_CMD_HELP, mysql-dump) Сохраняем Dump таблицы для Developing
	$(call SHOW_CMD_HELP, mysql-stop) Остановлеваем приложение \(MySQL\)
	$(call SHOW_CMD_HELP, mysql-rm) удаляем остановленное приложение \(MySQL\)
	$(call SHOW_CMD_HELP, mysql-clean) очищаем окружение
	$(call SHOW_CMD_HELP, mysql-cli) подключение консольным клиентом

mysql-up:
	@docker-compose -p $(PROJECT_NAME) -f $(PROJECT_COMPOSE_FILE) up -d mysql

mysql-start:
	@docker-compose -p $(PROJECT_NAME) -f $(PROJECT_COMPOSE_FILE) start mysql

mysql-restore:
	$(call SHOW_TITLE_TARGET, MYSQL-RESTORE)
	@docker-compose -p $(PROJECT_NAME) -f $(PROJECT_COMPOSE_FILE) exec -T mysql /usr/bin/mysql -uroot -p$(SERVICE_MYSQL_ROOT_PASSWORD) $(SERVICE_MYSQL_DATABASE) < $(MYSQL_DUMP_FILE)

mysql-dump:
	$(call SHOW_TITLE_TARGET, MYSQL-DUMP)
	@mkdir -p $(PROJECT_DUMP_PATH)
	@docker-compose -p $(PROJECT_NAME) -f $(PROJECT_COMPOSE_FILE) exec mysql /usr/bin/mysqldump -uroot -p$(SERVICE_MYSQL_ROOT_PASSWORD) $(SERVICE_MYSQL_DATABASE) | tail -n +2 > $(MYSQL_DUMP_FILE)

mysql-stop:
	@docker-compose -p $(PROJECT_NAME) -f $(PROJECT_COMPOSE_FILE) stop mysql

mysql-rm:
	@docker-compose -p $(PROJECT_NAME) -f $(PROJECT_COMPOSE_FILE) rm -s mysql

mysql-down: mysql-stop mysql-rm

mysql-cli:
	@docker-compose -p $(PROJECT_NAME) -f $(PROJECT_COMPOSE_FILE) exec mysql /usr/bin/mysql -uroot -p$(SERVICE_MYSQL_ROOT_PASSWORD) $(SERVICE_MYSQL_DATABASE)

mysql-clean:
	@rm $(MYSQL_DUMP_FILE)

mysql-env:
	@echo "# ---- MYSQL ----" >> $(ENV_PATH)/.env
	@echo "SERVICE_MYSQL_ROOT_PASSWORD="$(SERVICE_MYSQL_ROOT_PASSWORD) >> $(ENV_PATH)/.env
	@echo "SERVICE_MYSQL_DATABASE="$(SERVICE_MYSQL_DATABASE) >> $(ENV_PATH)/.env
	@echo "SERVICE_MYSQL_USER="$(SERVICE_MYSQL_USER) >> $(ENV_PATH)/.env
	@echo "SERVICE_MYSQL_PASSWORD="$(SERVICE_MYSQL_PASSWORD) >> $(ENV_PATH)/.env
	@echo "MYSQL_DUMP_FILE="$(MYSQL_DUMP_FILE) >> $(ENV_PATH)/.env
	@echo "" >> .env
